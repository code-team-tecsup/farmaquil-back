from rest_framework import serializers
from kardex.models import  DtoKardex, Abastecimiento, DtoTicket


#CREAMOS LA SERIALIZACIÓN,  LOS CAMPOS SE VAN A DEVOLVER
class DtoKardexSerializer(serializers.ModelSerializer):
    class Meta:
        model=DtoKardex
        fields=('item','id','iddetalle','tipo','fecha','serie','numero','lote','vencimiento','entrada','salida','saldo')

#CREAMOS LA SERIALIZACIÓN,LOS CAMPOS SE VAN A DEVOLVER
class AbastecimientoSerializer(serializers.ModelSerializer):
    class Meta:
        model=Abastecimiento
        fields=('origen','destino','costo','tiempo','condicion','promedio')

#CREAMOS LA SERIALIZACIÓN, LOS CAMPOS QUE SE VA A DEVOLVER
class DtoTicketsSerializer(serializers.ModelSerializer):
    class Meta:
        model=DtoTicket
        fields=('idticket','fecha','titulo','prioridad','usuarioOrigen')
