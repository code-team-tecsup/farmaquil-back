from djongo import models
# Create your models here.
#SE CREAN LOS MODELOS DE ACUERDO A NUESTRA BASE DE DATOS

#MAPEO DE LA COLECION TIPO
class Tipo(models.Model):
    idtipo=models.IntegerField()
    descripcion=models.TextField()
    class Meta: db_table = 'tipos'

#MAPEO DEL DOCUMENTO EMBEBIDO EN LA COLECION REGISTRO_ENTRADAS
class detalleRegistroEntradas(models.Model):
    iddetalle=models.IntegerField()
    idproducto = models.TextField()
    cantidad=models.IntegerField()
    lote = models.TextField()
    vencimiento = models.TextField()
    iddisposicion = models.IntegerField()
    created_at = models.DateField()
    updated_at = models.DateField()
    class Meta: abstract = True

#MAPEO DE LA COLECION REGISTRO ENTRADAS
class RegistroEntrada(models.Model):
    identrada=models.IntegerField()
    idalmacen = models.IntegerField()
    iddocumento=models.TextField()
    idusuario = models.IntegerField()
    serie = models.TextField()
    numero = models.TextField()
    fecha=models.DateField()
    created_at = models.DateField()
    updated_at = models.DateField()
    detalleRegistroEntradas =  models.ArrayField(model_container=detalleRegistroEntradas)
    class Meta: db_table = 'registro_entradas'

#MAPEO DEL DOCUMENTO EMBEBIDO EN LA COLECION REGISTRO_SALIDAS
class detalleRegistroSalidas(models.Model):
    iddetalle=models.IntegerField()
    idproducto = models.TextField()
    cantidad=models.IntegerField()
    lote = models.TextField()
    vencimiento = models.TextField()
    iddisposicion = models.IntegerField()
    created_at = models.DateField()
    updated_at = models.DateField()
    class Meta: abstract = True

#MAPEO DE LA COLECION REGISTRO SALIDA
class RegistroSalida(models.Model):
    idsalida=models.IntegerField()
    idalmacen = models.IntegerField()
    iddocumento=models.TextField()
    idusuario = models.IntegerField()
    serie = models.TextField()
    numero = models.TextField()
    fecha=models.DateField()
    created_at = models.DateField()
    updated_at = models.DateField()
    detalleRegistroSalidas=  models.ArrayField(model_container=detalleRegistroSalidas)
    class Meta: db_table = 'registro_salidas'

#MAPEO DE LA COLECION PERIODO
class Periodo(models.Model):
    idperiodo=models.IntegerField()
    descripcion = models.TextField()
    inicio=models.DateField()
    fin = models.DateField()
    class Meta: db_table = 'periodos'

#CLASE DTO PARA DEVOLVER EL RESULTADO DE DEL KARDEX PROCESADO
class DtoKardex(models.Model):
    id = models.IntegerField
    iddetalle = models.IntegerField
    fecha=models.DateField
    created_at = models.DateField
    serie=models.TextField
    numero =models.TextField
    lote = models.TextField
    vencimiento = models.TextField
    entrada = models.IntegerField
    salida = models.IntegerField
    saldo = models.IntegerField
    item = models.IntegerField
    tipo=models.TextField

#MAPEO DE LA COLECION TICKETS
class Ticket(models.Model):
    idticket = models.IntegerField()
    idprioridad = models.IntegerField()
    fecha = models.DateField()
    titulo = models.TextField()
    contenido = models.TextField()
    idusuarioOrigen = models.IntegerField()
    idusuarioDestino = models.IntegerField()
    atendido = models.BooleanField()
    class Meta: db_table = 'tickets'


class Usuario(models.Model):
    idusuario = models.IntegerField()
    nombres = models.TextField()
    class Meta: db_table = 'usuarios'

class DtoTicket(models.Model):
    idticket = models.IntegerField()
    titulo = models.TextField()
    fecha = models.DateField()
    prioridad = models.IntegerField()
    usuarioOrigen=models.TextField()

#MAPEO DE LA COLECION ABASTECIMIENTO
class Abastecimiento(models.Model):
    origen = models.TextField()
    destino = models.TextField()
    costo = models.IntegerField()
    tiempo = models.IntegerField()
    condicion = models.IntegerField()
    promedio = models.DecimalField(max_digits=10,decimal_places=2)
    class Meta: db_table = 'abastecimiento'
