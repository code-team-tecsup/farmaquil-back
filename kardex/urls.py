
from django.contrib import admin
from django.urls import path

from kardex.views import Kardex, ListaAbastecimiento, ListaTicketPrioridad

urlpatterns = [
    #SE CREA LA RUTA KARDEX QUE RECIBE TRES PARAMETROS
    path('kardex/<codigoAlmacen>/<codigoPeriodo>/<codigoProducto>', Kardex.as_view(), name='kardex'),
    #ABASTECIMINENTO NO RECIBE PARAMETROS
    path('abastecimiento/', ListaAbastecimiento.as_view(), name='abastecimiento'),

    #PRIORIDAD RECIBE EL ID DEL USUARIO
    path('prioridad/<codigoUsuario>',ListaTicketPrioridad.as_view(),name='lista-prioridad'),

]
