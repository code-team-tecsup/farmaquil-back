# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from kardex.models import Ticket ,RegistroEntrada, DtoKardex, Periodo, RegistroSalida, Abastecimiento, DtoTicket, Usuario
from kardex.serializers import DtoKardexSerializer, AbastecimientoSerializer, DtoTicketsSerializer


class Kardex(APIView):
    #METODO GET QUE OBTIENE 3 PARAMETROS
    def get(self,request,codigoAlmacen,codigoPeriodo,codigoProducto):
        #VARIABLE DONDE SE ALMACENARA LOS MOVIMIENTOS DEL PRODUCTO EN EL ALMACEN Y PERIODO INDICADO
        listaKardex = []

        #SE OBTIENE EL PERIODO SOLICITADO
        periodo=Periodo.objects.filter(idperiodo=codigoPeriodo).first()

        #SE OBTIENE TODAS LAS ENTRADAS DEL PRODUCTO EN EL ALMACEN Y PERIODO INDICADOS
        listaEntrada=RegistroEntrada.objects.filter(idalmacen=codigoAlmacen,detalleRegistroEntradas={'idproducto':codigoProducto})
        #SE OBTIENE TODAS LAS SALIDAS DEL PRODUCTO EN EL ALMACEN Y PERIODO INDICADOS
        listaSalida = RegistroSalida.objects.filter(idalmacen=codigoAlmacen,created_at__gte=periodo.inicio,created_at__lte=periodo.fin,detalleRegistroSalidas={'idproducto': codigoProducto})

        #SE RECORRE LAS ENTRADAS OBTENIDAS
        for regentrada in listaEntrada:
            #SE OBTIENE EL DETALLE DEL DOCUMENTO EMBEBIDO
            listaDetalle=regentrada.detalleRegistroEntradas
            #SE RECORRE EL DETALLE OBTENIDO
            for detalle in listaDetalle:
                #SE OBTIENE TODOS LOS DETALLES DEL DOCUMENTO EMBEBIDO
                #NOS ASEGURAMOS DE OBTENER LOS DATOS DEL PRODUCTO BUSCADO
                if(detalle['idproducto']==codigoProducto):
                    #SE INSTANCIA LA CLASE DTO Y SE LLENAN SUS ATRIBUTOS SEGUN CORRESPONDA
                    dtoKardex = DtoKardex()
                    dtoKardex.tipo="ENTRADA"
                    dtoKardex.id = regentrada.identrada
                    dtoKardex.iddetalle = detalle['iddetalle']
                    dtoKardex.fecha = regentrada.fecha
                    dtoKardex.created_at = regentrada.created_at
                    dtoKardex.serie = regentrada.serie
                    dtoKardex.numero = regentrada.numero
                    dtoKardex.lote = detalle['lote']
                    dtoKardex.vencimiento = detalle['vencimiento']
                    dtoKardex.entrada = detalle['cantidad']
                    dtoKardex.salida = 0
                    dtoKardex.saldo = 0
            #SE AGREGA EL DTO A LA VARIABLE KARDEX
            listaKardex.append(dtoKardex)

        # SE RECORRE LAS SALIDAS OBTENIDAS
        for regsalida in listaSalida:
            # SE OBTIENE EL DETALLE DEL DOCUMENTO EMBEBIDO
            # NOS ASEGURAMOS DE OBTENER LOS DATOS DEL PRODUCTO BUSCADO
            listaDetalle=regsalida.detalleRegistroSalidas
            for detalle in listaDetalle:
                if(detalle['idproducto']==codigoProducto):
                    #SE INSTANCIA LA CLASE DTO Y SE LLENAN SUS ATRIBUTOS SEGUN CORRESPONDA
                    dtoKardex = DtoKardex()
                    dtoKardex.tipo = "SALIDA"
                    dtoKardex.id=regsalida.idsalida
                    dtoKardex.iddetalle = detalle['iddetalle']
                    dtoKardex.fecha = regsalida.fecha
                    dtoKardex.created_at = regsalida.created_at
                    dtoKardex.serie = regsalida.serie
                    dtoKardex.numero = regsalida.numero
                    dtoKardex.lote = detalle['lote']
                    dtoKardex.vencimiento = detalle['vencimiento']
                    dtoKardex.entrada = 0
                    dtoKardex.salida = detalle['cantidad']
                    dtoKardex.saldo = 0
            #SE AGREGA EL DTO A LA VARIABLE KARDEX
            listaKardex.append(dtoKardex)

        #EN CASO NO EXISTAN MOVIMIENTOS SE CREA UNO PARA INDICAR AL USUARIO QUE HAY MOVIMIENTOS
        if listaKardex==[]:
            dtoKardex = DtoKardex()
            dtoKardex.tipo = "ENTRADA"
            dtoKardex.id =0
            dtoKardex.iddetalle = 0
            dtoKardex.fecha = periodo.inicio
            dtoKardex.created_at = periodo.inicio
            dtoKardex.serie = "SIN"
            dtoKardex.numero = "MOVIMIENTO"
            dtoKardex.lote = ""
            dtoKardex.vencimiento =""
            dtoKardex.entrada = 0
            dtoKardex.salida = 0
            dtoKardex.saldo = 0
            listaKardex.append(dtoKardex)

        #SE ORDENA EL KARDEX CRONOLOGIMANTE
        listaKardex.sort(key=lambda x: x.created_at)

        #SE RECALCULA EL SALDO
        saldoKardex=0
        item=0
        for kardex in listaKardex:
            item=item+1
            if(kardex.tipo=="ENTRADA"):
                saldoKardex = saldoKardex + kardex.entrada
            else:
                saldoKardex = saldoKardex - kardex.salida

            kardex.item=item
            kardex.saldo=saldoKardex

        #SE REALIZA LA SERIALIZACIÓN DEL KARDEX ORDENADO
        respuesta_json = DtoKardexSerializer(listaKardex ,many=True)
        #SE RESPONDE AL USUARIO
        return Response(respuesta_json.data)

class ListaAbastecimiento(APIView):
    def get(self, request):
        listaAbastecimiento = Abastecimiento.objects

        respuesta_json = AbastecimientoSerializer(listaAbastecimiento, many=True)

        return Response(respuesta_json.data)

class ListaTicketPrioridad(APIView):
    def get(self, request,codigoUsuario):
        # COLA DONDE SE ALMACENARA LOS TICKET ORDENADOS POR PRIORIDAD
        cola = []
        colaPrioridad = []

        listaTickets=Ticket.objects.filter(idusuarioDestino=codigoUsuario, atendido=False)
        for ticket in listaTickets:
            usuario=Usuario.objects.filter(idusuario=ticket.idusuarioOrigen).first()

            dtoTicket=DtoTicket()
            dtoTicket.idticket=ticket.idticket
            dtoTicket.titulo=ticket.titulo
            dtoTicket.fecha=ticket.fecha
            dtoTicket.usuarioOrigen=usuario.nombres
            dtoTicket.prioridad = ticket.idprioridad
            cola.append(dtoTicket)

        colaPrioridad = sorted(cola, key=lambda x: x.prioridad)


        respuesta_json = DtoTicketsSerializer(colaPrioridad, many=True)

        return Response(respuesta_json.data)